from django.apps import AppConfig


class PuppiesConfig(AppConfig):
    name = 'puppies'
    verbose_name = 'Puppies'